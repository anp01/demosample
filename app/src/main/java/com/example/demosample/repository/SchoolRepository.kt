package com.example.demosample.repository

import com.example.demosample.api.ApiHelper
import javax.inject.Inject

class SchoolRepository @Inject constructor(private val apiHelper: ApiHelper) {

    suspend fun fetchSchoolList() = apiHelper.fetchSchoolList()

    suspend fun fetchSatScore(dbn: String) = apiHelper.fetchSatScore(dbn)
}