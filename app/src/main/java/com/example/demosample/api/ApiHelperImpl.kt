package com.example.demosample.api

import com.example.demosample.models.SchoolResponse
import com.example.demosample.models.details.SchoolDetailsResponse
import retrofit2.Response
import javax.inject.Inject

class ApiHelperImpl @Inject constructor(private val apiService: ApiService) : ApiHelper {

    override suspend fun fetchSchoolList(): Response<SchoolResponse> {
        return apiService.fetchSchoolList()
    }

    override suspend fun fetchSatScore(dbn: String): Response<SchoolDetailsResponse> {
        return apiService.fetchSatScore(dbn)
    }
}