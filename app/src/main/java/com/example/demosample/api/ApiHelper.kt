package com.example.demosample.api

import com.example.demosample.models.SchoolResponse
import com.example.demosample.models.details.SchoolDetailsResponse
import retrofit2.Response

interface ApiHelper {

    suspend fun fetchSchoolList(): Response<SchoolResponse>

    suspend fun fetchSatScore(dbn: String): Response<SchoolDetailsResponse>
}