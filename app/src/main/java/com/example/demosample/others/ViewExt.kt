package com.example.demosample.others

import android.view.View

object ViewExt {

    /**
     * Kotlin extension to show view
     */
    fun View.show() {
        this.visibility = View.VISIBLE
    }

    /**
     * Kotlin extension to hide view
     */
    fun View.hide() {
        this.visibility = View.GONE
    }
}