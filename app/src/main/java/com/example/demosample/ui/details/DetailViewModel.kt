package com.example.demosample.ui.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.demosample.models.details.SchoolDetailsResponse
import com.example.demosample.others.Resource
import com.example.demosample.repository.SchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(private val schoolRepository: SchoolRepository) :
    ViewModel() {

    val satScoreResponse = MutableLiveData<Resource<SchoolDetailsResponse>>()

    fun fetchSatScore(dbn: String) {
        viewModelScope.launch {
            try {
                satScoreResponse.postValue(Resource.Loading())
                val response = schoolRepository.fetchSatScore(dbn)
                if (response.isSuccessful)
                    satScoreResponse.postValue(Resource.Success(response.body()))
                else
                    satScoreResponse.postValue(Resource.Error("Some thing went wrong"))
            } catch (e: Exception) {
                satScoreResponse.postValue(Resource.Error(e.localizedMessage))
            }
        }
    }
}