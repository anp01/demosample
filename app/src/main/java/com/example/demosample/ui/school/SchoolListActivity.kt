package com.example.demosample.ui.school

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.demosample.R
import com.example.demosample.adapter.SchoolListAdapter
import com.example.demosample.databinding.ActivitySchoolListBinding
import com.example.demosample.others.Resource
import com.example.demosample.others.ViewExt.hide
import com.example.demosample.others.ViewExt.show
import com.example.demosample.ui.details.SchoolDetailsActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolListActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySchoolListBinding
    private lateinit var schoolListAdapter: SchoolListAdapter
    private val mainViewModel: SchoolListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySchoolListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupToolbar()
        initSchoolListAdapter()
        setupRecyclerview()
        observeViewModel()
    }

    /**
     * Display toolbar title and enable navigate back icon
     */
    private fun setupToolbar() {
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            title = getString(R.string.new_york_school_list)
        }
    }

    /**
     * Observer ViewModel data and update UI based on the data changes
     */
    private fun observeViewModel() {
        mainViewModel.schoolListResponse.observe(this) { resource ->
            when (resource) {
                is Resource.Loading -> {
                    binding.apply {
                        recyclerView.hide()
                        progressBar.show()
                        errorTextView.hide()
                    }
                }

                is Resource.Success -> {
                    binding.apply {
                        recyclerView.show()
                        progressBar.hide()
                        errorTextView.hide()
                    }

                    schoolListAdapter.submitList(resource.data)
                }

                is Resource.Error -> {
                    binding.apply {
                        recyclerView.hide()
                        progressBar.hide()
                        errorTextView.show()

                        errorTextView.text = resource.message
                    }
                }
            }
        }
    }

    /**
     * Init schoolListAdapter and open SchoolDetailsActivity on school selection
     */
    private fun initSchoolListAdapter() {
        schoolListAdapter = SchoolListAdapter { selectedSchool ->
            val intent = Intent(this, SchoolDetailsActivity::class.java)

            val bundle = Bundle().also {
                it.putParcelable(SchoolDetailsActivity.SCHOOL_DETAILS_INTENT, selectedSchool)
            }
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }

    /**
     * init and setup recycler view
     */
    private fun setupRecyclerview() {
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@SchoolListActivity)
            setHasFixedSize(true)
            adapter = schoolListAdapter
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}