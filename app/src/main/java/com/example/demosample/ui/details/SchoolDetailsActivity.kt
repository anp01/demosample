package com.example.demosample.ui.details

import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.demosample.R
import com.example.demosample.databinding.ActivitySchoolDetailsBinding
import com.example.demosample.models.SchoolResponseItem
import com.example.demosample.models.details.SchoolDetailsResponse
import com.example.demosample.others.Resource
import com.example.demosample.others.ViewExt.hide
import com.example.demosample.others.ViewExt.show
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySchoolDetailsBinding
    private val detailViewModel: DetailViewModel by viewModels()
    private var selectedSchool: SchoolResponseItem? = null

    companion object {
        const val SCHOOL_DETAILS_INTENT = "school_details_intent"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySchoolDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupToolbar()

        val bundle = intent.extras
        selectedSchool = bundle?.getParcelable(SCHOOL_DETAILS_INTENT)
        selectedSchool?.dbn?.let { detailViewModel.fetchSatScore(it) }

        binding.schoolNameText.text = selectedSchool?.school_name
        observeViewModel()
    }

    /**
     * set toolbar title and enable navigate back button in toolbar
     */
    private fun setupToolbar() {
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            title = getString(R.string.school_details)
        }
    }

    private fun observeViewModel() {
        detailViewModel.satScoreResponse.observe(this) { response ->
            when (response) {
                is Resource.Loading -> {
                    binding.apply {
                        progressBar.show()
                        detailLayout.hide()
                        errorTextview.hide()
                    }
                }

                is Resource.Error -> {
                    binding.apply {
                        progressBar.hide()
                        detailLayout.hide()
                        errorTextview.show()
                    }
                }

                is Resource.Success -> {
                    binding.apply {
                        progressBar.hide()
                        detailLayout.show()
                        errorTextview.hide()
                    }

                    displaySchoolDetails(response.data)
                }
            }
        }
    }

    /**
     * Display selected school details (Average Math, Reading and Writing score) if data available
     * else display No SAT score available message
     */
    private fun displaySchoolDetails(schoolData: SchoolDetailsResponse?) {
        if (schoolData != null && schoolData.isNotEmpty()) {
            val data = schoolData[0]
            binding.apply {
                avgMathScoreTextView.text = data.sat_math_avg_score
                avgReadingScoreTextView.text = data.sat_critical_reading_avg_score
                avgWritingScoreTextView.text = data.sat_writing_avg_score
            }
        } else {
            binding.apply {
                detailLayout.hide()
                errorTextview.show()
                errorTextview.text = getString(R.string.no_content_available)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}